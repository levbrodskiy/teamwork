package ru;

import ru.blm.mobileoperator.*;
import ru.ei.toByteCode.*;

public class System {
    public static void main(String[] args) {
        HumanResourcesDepartment humanResourcesDepartment = new HumanResourcesDepartment();
        AdvertizingDepartment advertizingDepartment = new AdvertizingDepartment();
        EquipmentMaintenance equipmentMaintenance = new EquipmentMaintenance();
        FinancialAccounting financialAccounting = new FinancialAccounting();
        ProcessingOfSignals processingOfSignals = new ProcessingOfSignals();
        Compilation compilation = new Compilation();
        Interpritator interpritator = new Interpritator();
        Loader loader = new Loader();
        SemanticAnaliz semanticAnaliz = new SemanticAnaliz();
        ServerPart serverPart = new ServerPart();
        SintacsisAnalyz sintacsisAnalyz = new SintacsisAnalyz();
        Translator translator = new Translator();
    }
}
